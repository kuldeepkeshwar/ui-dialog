'use strict';

describe('Directive: modalFooter', function () {

  // load the directive's module
  beforeEach(module('uiDialogApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<modal-footer></modal-footer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the modalFooter directive');
  }));
});
