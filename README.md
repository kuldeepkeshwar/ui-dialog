# ui-dialog
## This is a service to quickly create modal windows. This service supports 3 type of modals




* **Alert**
* **Confirm**
* **Dialog**

## Usage
  
  
  - Alert
  
```
    angular.module('myApp',['uiDialogApp']);
    angular.module('myApp').controller("myCtrl",function(dialog){
       var alertOptions={
          content:'Custom alert content!!',
          title:'Custom Alert !!',
          btn:{
             label:"Ok",
             cls:'btn-primary',
             handler:function(modal){
               modal.close();
             }
         }
       };
       dialog.alert(alertOptions);
    });
```

| Property     | type    | Description |
| --------|---------|-------|
| title  | string   | title for alert box   |
| content | string | message to show in alert box    |
| btn | object(optional) |  to configure 'Ok' button in alert box|

  
  
  - Confirm
  
```angular.module('myApp',['uiDialogApp']);
    angular.module('myApp').controller("myCtrl",function(dialog){
       var confirmOptions={
          content:"Custom confirm !!",
          title:"custom confirm title!!",
          acceptBtn:{
               label:'Yes',
               cls:'btn-success',
               handler:function(modal){
    	     console.log('You clicked Yes button!!')
    	     modal.colse();
               }
           },
           rejectBtn:{
               label:'No',
               cls:'btn-link',
               handler:function(modal){
    	     console.log('You clicked No button!!')
    	     modal.colse();
    	   }
           }
       };
       dialog.confirm(confirmOptions);
    });
```

| Property     | type    | Description |
| --------|---------|-------|
| title  | string   | title for confirm box   |
| content | string | message to show in confirm box    |
| acceptBtn | object(optional) |  to configure 'Accept' button in confirm box|
| rejectBtn | object(optional) |  to configure 'Reject' button in confirm box|
  

  - Dialog
  
```angular.module('myApp',['uiDialogApp']);
    angular.module('myApp').controller("myCtrl",function(dialog){
       var dialogOptions={
          backdrop:'static',
          keyboard: false,
          size:'sm',
          content:"This is my custom dialog",
          header:{
            title:'Custom dialog title',
            closeBtn:true
          },
          footerBtn:[{
            label:'Button one',
            cls:'btn-primary',
            handler:function(modal){
              console.log('You clicked button one');
              modal.close();
            }
          },{
            label:'Button Two',
            cls:'btn-info',
            handler:function(modal){
              console.log('You clicked button two');
              modal.close();
            }
          },{
            label:'Button three',
            cls:'btn-warning',
            handler:function(modal){
              console.log('You clicked button three');
              modal.close();
            }
          }]
       };
       dialog.dialog(dialogOptions);
    });
```

| Property     | type    | Description |
| --------|---------|-------|
| header  | object   | header configuration for dialog box   |
| content | string | message to show in confirm box    |
| size | string |  optional size of dialog. Allowed values: 'sm' (small) or 'lg' (large). Requires Bootstrap 3.1.0 or later|
| backdrop | string/boolean |  controls presence of a backdrop. Allowed values: true (default), false (no backdrop),'static' - backdrop is present but dialog is not closed when clicking outside of the dialog|
| keyboard | boolean |  indicates whether the dialog should be closable by hitting the ESC key,defaults to false|
| footerBtn | array |  used to show buttons in dialog footer|



  - configuration for button in dialog

| Property     | type    | Description |
| --------|---------|-------|
| label  | string   | label for button   |
| cls | string/array | can be space separated string of class or array of classes    |
| handler | function |  handler function which will be call on click of button ,handler function is argumented with modal instance which has a close function which can be use to close the alert box|
