'use strict';

/**
 * @ngdoc function
 * @name uiDialogApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the uiDialogApp
 */
angular.module('uiDialogApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
