'use strict';

/**
 * @ngdoc function
 * @name uiDialogApp.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the uiDialogApp
 */
angular.module('uiDialogApp')
  .controller('ModalCtrl', ['$document','$scope','$modalInstance', 'options',function ($document,$scope, $modalInstance, options) {
    var _domTitle=$document[0].title;
    
    $scope.isHeader=function(h){
      return angular.isObject(h) && angular.isString(h.title) ;
    }

    $scope.invokeHandler = function (btn) {
      if($scope.showHeader){
        $document[0].title=_domTitle;
      }
      if(angular.isFunction(btn.handler)){
        btn.handler.call(null,$modalInstance);
      }
      //$modalInstance.close(true);
    };
    
    $scope.showHeader=$scope.isHeader(options.header);
    $scope.headerCloseBtn=($scope.showHeader)?options.header.closeBtn:false;
    
    if($scope.showHeader){
      $document[0].title=$scope.title=options.header.title;
    }
    $scope.msg = options.content;
    $scope.footerBtn=(angular.isArray(options.footerBtn))?options.footerBtn:[]; 
    $scope.getClass=function(cls){
      if(angular.isString(cls))
        return cls;
      if(angular.isArray(cls)){
        return cls.join(' ');
      }
    }
  }]);
