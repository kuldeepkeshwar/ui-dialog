'use strict';

/**
 * @ngdoc directive
 * @name uiDialogApp.directive:modalFooter
 * @description
 * # modalFooter
 */
angular.module('uiDialogApp')
  .directive('modalFooter', function () {
  	var template='<div class="modal-footer text-center" data-ng-transclude></div>';
    return {
      template:template,
		restrict: 'E',
	    transclude: true,
	    replace:true,
	    link: function (scope, element, attrs,controller,transclude) {
	    	//console.log('abstractModalBody');
	    	transclude(scope, function(clone, scope) {
		        element.html(clone);
		    });
	    }
    };
  });
