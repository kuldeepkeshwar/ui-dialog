'use strict';

/**
 * @ngdoc directive
 * @name uiDialogApp.directive:modalBody
 * @description
 * # modalBody
 */
angular.module('uiDialogApp')
  .directive('modalBody', function () {
  	var template='<div class="modal-body text-center" data-ng-transclude></div>';
    return {
      template:template,
		restrict: 'E',
	    transclude: true,
	    replace:true,
	    link: function (scope, element, attrs,controller,transclude) {
	    	//console.log('abstractModalBody');
	    	transclude(scope, function(clone, scope) {
		        element.html(clone);
		    });
	    }
    };
  });
