'use strict';

/**
 * @ngdoc directive
 * @name uiDialogApp.directive:modalHeader
 * @description
 * # modalHeader
 */
angular.module('uiDialogApp')
  .directive('modalHeader', function () {
  	var template='<div class="modal-header noBorder" >'+
				     '<button type="button" ng-show="$closeBtn" ng-click="$dismiss()" class="close" data-dismiss="modal" >'+
				     		'<span aria-hidden="true" class="icon-close-popup"></span><span class="sr-only">Close</span>'+
				     '</button>'+
				     '<div class="modal-title text-center" data-ng-transclude></div>'+
				 '</div>';
    return {
        template:template,
		restrict: 'E',
	    transclude: true,
	    replace:true,
	    link: function (scope, element, attrs,controller,transclude) {
	    	var _closeBtn=scope.$eval(attrs.closeBtn);
			scope.$closeBtn=(typeof _closeBtn==='boolean')?_closeBtn:false;
			transclude(scope, function(clone, scope) {
		        element.html(clone);
		    });
	    }
    };
  });
