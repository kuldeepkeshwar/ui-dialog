'use strict';

/**
 * @ngdoc service
 * @name uiDialogApp.dialog
 * @description
 * Service to show alert/confirm message.
 * @requires $modal
 * @requires templateURL
 * ## General usage
 * @example
  <example module="uiDialogApp">
   <file name="app.js">
     angular.module('uiDialogApp')
      .controller('TestController', ['$scope','dialog', function($scope,dialog) {
       	var config={
	    	  content:'Custom Alert!!!',
          title:'Custom Alert Title!!!',
          btn:{
            label:"Ok",
            handler:function(_m){
              _m.close();
            }
          }
	    	};
	    	$scope.showAlertDialog = function() {
	    	  dialog.alert();
	    	};
	    }]);
	    angular.module('uiDialogApp').controller('ModalCtrl', function ($document,$scope, $modalInstance, options) {
        var _domTitle=$document[0].title;
        
        $scope.isHeader=function(h){
          return angular.isObject(h) && angular.isString(h.title) ;
        }

        $scope.invokeHandler = function (btn) {
          if($scope.showHeader){
            $document[0].title=_domTitle;
          }
          if(angular.isFunction(btn.handler)){
            btn.handler.call(null,$modalInstance);
          }
        };
        
        $scope.showHeader=$scope.isHeader(options.header);
        $scope.headerCloseBtn=($scope.showHeader)?options.header.closeBtn:false;
        
        if($scope.showHeader){
          $document[0].title=$scope.title=options.header.title;
        }
        $scope.msg = options.content;
        $scope.footerBtn=(angular.isArray(options.footerBtn))?options.footerBtn:[]; 
        $scope.getClass=function(cls){
          if(angular.isString(cls))
            return cls;
          if(angular.isArray(cls)){
            return cls.join(' ');
          }
        }
      });
   </file>
   <file name="index.html">
     <div ng-controller="TestController">
       <button class="btn btn-default" ng-click="showAlertDialog()">Open me!</button><br>
       <script type="text/ng-template" id="/views/modal.html">
          <modal-header ng-show="showHeader" close-btn="headerCloseBtn">
              <h3>{{title}}</h3>
          </modal-header>
          <modal-body>
              <p>
                  {{msg}}
              </p>
          </modal-body>
          <modal-footer>
              <button class="btn {{getClass(btn.cls)}}" ng-click="invokeHandler(btn)" ng-repeat="btn in footerBtn">{{btn.label}}</button>
          </modal-footer>
       </script>
       <script type="text/ng-template" id="/views/abstractheader.html.html">
          <div class="modal-header noBorder" >
              <button type="button" ng-show="$closeBtn" ng-click="$dismiss()" class="close" data-dismiss="modal" ><span aria-hidden="true" class="icon-close-popup"></span><span class="sr-only">Close</span></button>
              <div class="modal-title" data-ng-transclude></div>
          </div>  
       </script>
       <script type="text/ng-template" id="/views/abstractbody.html.html">
          <div class="modal-body text-center" data-ng-transclude></div>
       </script>
       <script type="text/ng-template" id="/views/modal-footer.html">
          <div class="modal-footer" data-ng-transclude></div>
       </script>
     </div>
   </file>
 </example>

*/
angular.module('uiDialogApp')
  .service('dialog', ['$modal','$log',function ($modal,$log) {
  	var defaults={
  		buttons:{
  			BUTTON_OK:'Ok',
        BUTTON_YES:'Yes',
  			BUTTON_NO:'No',
    		BUTTON_CLS_PRIMARY:"btn-primary",
    		BUTTON_CLS_LINK:"btn-link"
  		}	
  	};
  	var defaultDialogOptions={
  		content:"",
        header:{
          title:undefined,
          closeBtn:false,
        },
        footerBtn:[]
  	};
  	var _$defaultDialogOptions={
  		templateUrl: 'dist/views/modal.html',
        controller: 'ModalCtrl',
        backdrop:'static' ,
        keyboard: false,
        size:undefined
  	};
    function _defaultHandler(_m){
        _m.close();
    }
    function defaultDialogConfig(){
      return angular.extend({},_$defaultDialogOptions);
    }
    function defaultModalConfig(options){
      return angular.extend({},_$defaultDialogOptions,defaultDialogOptions,options);
    }
    function defaultAlertConfig(options){
      /*
        options={
          content:"",
          title:""
          btn:{
            label:"",
            handler:function(){}
          }
        }
      */
      var _options={
          content:"",
          title:"",
          btn:{
            label:defaults.buttons.BUTTON_OK,
            handler:_defaultHandler
          }
        };
      angular.extend(_options,options);
      var _config={};
      _config.content=_options.content;
      _config.header={
        title:_options.title,
        closeBtn:false,
      };
      _config.footerBtn=[{
        label:_options.btn.label,
        cls:defaults.buttons.BUTTON_CLS_PRIMARY,
        handler:_options.btn.handler||_defaultHandler
      }];
      return angular.extend(defaultModalConfig(_options),_config);
    }
    function  defaultConfirmConfig(options){
      /*
        options={
          content:"",
          title:""
          acceptBtn:{
            label:"",
            handler:function(){}
          },
          rejecttBtn:{
            label:"",
            handler:function(){}
          }
        }
      */
      var _options={
          content:"",
          title:"",
          acceptBtn:{
            label:defaults.buttons.BUTTON_YES,
            cls:defaults.buttons.BUTTON_CLS_PRIMARY,
            handler:_defaultHandler
          },
          rejectBtn:{
            label:defaults.buttons.BUTTON_NO,
            cls:defaults.buttons.BUTTON_CLS_LINK,
            handler:_defaultHandler
          }
        };
      angular.extend(_options,options);  
      var _config={};
      _config.content=_options.content;
      _config.header={
        title:_options.title,
        closeBtn:false,
      };
      _config.footerBtn=[];
      _config.footerBtn.push({
        label:_options.acceptBtn.label,
        cls:defaults.buttons.BUTTON_CLS_PRIMARY,
        handler:_options.acceptBtn.handler
      });
      _config.footerBtn.push({
        label:_options.rejectBtn.label,
        cls:defaults.buttons.BUTTON_CLS_LINK,
        handler:_options.rejectBtn.handler
      });
      return angular.extend(defaultModalConfig(_options),_config);
    };
  /**
   * @ngdoc method
   * @name dialog
   * @methodOf uiDialogApp.dialog
   * @param {Object} config object to configure the dialog.<br>
   *  config{Object} can have following properties<br>
   *  content : Dialog message<br>
   *  header:{<br>
          title:'Dialog title',<br>
          closeBtn: can be true/false to show/hide close button in dialog header<br>
      },<br>
   *  backdrop:controls presence of a backdrop. Allowed values: true , false(no backdrop), 'static' - backdrop is present but modal window is not closed when clicking outside of the modal window, defaults to 'static'<br>
   *  keyboard: indicates whether the dialog should be closable by hitting the ESC key, defaults to true<br>
   *  size:optional size of modal window. Allowed values: 'sm' (small) or 'lg' (large). Requires Bootstrap 3.1.0 or later, defaults to 'sm'<br>
   *  footerBtn: An array of buttons,<br>
   *             sample button object : {<br>
   *                   label:'label for button',<br>
   *                   cls: can be array or string of class e.g ['btn-primary','btn-red'] or 'btn-primary btn-red' ,<br>
   *                   handler: handler function for button ,modal instance is passed in while invoking the handler<br>
   *                }
   *        
   * @description used to show alert dialog on screen
   * @returns {Object} a modal instance, an object with the following properties:<br>
   *  close(result) - a method that can be used to close a modal, passing a result<br>
   *  dismiss(reason) - a method that can be used to dismiss a modal, passing a reason  
  */  
    this.dialog=function(config){
      var _config=angular.extend(defaultDialogConfig(),config);
      return this.$init(_config);
    }
  /**
   * @ngdoc method
   * @name alert
   * @methodOf uiDialogApp.dialog
   * @param {Object} config object to configure the alert.<br>
   *	title:Alert dialog title<br>
   *	content : Alert  dialog message<br>
   * 	btn:{<br>
            label:"button label",<br>
            handler:function(){<br>
              //handler function , modal instance is passed in while invoking the handler
            }<br>
          }    	<br>
   * @description used to show alert dialog on screen
   * @returns {Object} a modal instance, an object with the following properties:<br>
   * 	close(result) - a method that can be used to close a modal, passing a result<br>
   *	dismiss(reason) - a method that can be used to dismiss a modal, passing a reason	
  */
    
    this.alert=function(config){
      var _config=defaultAlertConfig(config);
      return this.$init(_config);
    };
    
   /**
   * @ngdoc method
   * @methodOf uiDialogApp.dialog
   * @name confirm
   * @param {Object} config object to configure the confirm.<br>
   *  title:Confirm dialog title<br>
   *  content : Confirm  dialog message<br>
   *  acceptBtn:{<br>
            label:"button label",<br>
            handler:function(){<br>
              //handler function , modal instance is passed in while invoking the handler
            }<br>
      },<br>
      rejectBtn:{<br>
            label:"button label",<br>
            handler:function(){<br>
              //handler function , modal instance is passed in while invoking the handler
            }<br>
      }
   * @description used to show confirm dialog on screen
   * @returns {Object} a modal instance, an object with the following properties:<br>
   * 	close(result) - a method that can be used to close a modal, passing a result<br>
   *	dismiss(reason) - a method that can be used to dismiss a modal, passing a reason	
   */
    this.confirm=function(config){
      var _config=defaultConfirmConfig(config);
      return this.$init(_config);
    };
    this.$init=function(config){
      var _resolver={
        resolve: {
          options:function(){
              return config;
          }
        }
      };
      angular.extend(config,_resolver);
      var _modalInstance = $modal.open(config);
      _modalInstance.result.then(function (result) {
        $log.info('Modal closed ');
      }, function () {
        $log.info('Modal dismissed ');
      });
      return _modalInstance; 
    };      
}]);
