'use strict';

/**
 * @ngdoc overview
 * @name uiDialogApp
 * @description
 * # uiDialogApp
 *
 * Main module of the application.
 */
angular.module('uiDialogApp', ['ui.bootstrap']);
